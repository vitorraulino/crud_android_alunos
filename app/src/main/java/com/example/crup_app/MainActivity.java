package com.example.crup_app;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText et_cod;
    EditText et_nome;
    EditText et_nota;
    private SQLiteDatabase conn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.et_cod = findViewById(R.id.codET);
        this.et_nome = findViewById(R.id.nomET);
        this.et_nota = findViewById(R.id.notET);

        this.conn = openOrCreateDatabase("alunos",MODE_PRIVATE,null);
        conn.execSQL("CREATE TABLE IF NOT EXISTS notas" +
                     "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                     " nome VARCHAR NOT NULL," +
                     " nota DECIMAL NOT NULL)");
    }
    public void cadastrarOnClick(View v){
        ContentValues rg = new ContentValues();
        rg.put("nome",this.et_nome.getText().toString());
        rg.put("nota",Double.parseDouble(this.et_nota.getText().toString()));
        conn.insert("notas",null,rg);
        Toast.makeText(this,"Aluno cadastrado com sucesso",Toast.LENGTH_LONG).show();
    }
    public void alterarOnClick(View v){
        ContentValues rg = new ContentValues();
        rg.put("nome",this.et_nome.getText().toString());
        rg.put("nota",Double.parseDouble(this.et_nota.getText().toString()));
        conn.update("notas", rg,"id =" + et_cod.getText().toString(),null);
        Toast.makeText(this,"Aluno atualizado com sucesso",Toast.LENGTH_LONG).show();
    }
    public void excluirrOnClick(View v){

    }
    public void pesquisarOnClick(View v){

    }
    public void listarOnClick(View v){

    }
}
